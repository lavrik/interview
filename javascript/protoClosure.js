'use strict';

/*
Создать функцию-конструктор AccountController с защищенным свойством deposit,
которое можно получить с помощью метода getDeposit, но нельзя изменить извне.
*/

function AccountController() {

}

const account = new AccountController();

console.log(account.getDeposit()); // вернут текущий депозит

account.deposit = 9999; // не дожно изменить депозит
