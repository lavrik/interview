'use strict';

/*
Вызов функции с контекстом.
Необходимо описать все возможные способы вызова функции counter с контекстом ctx
*/

const ctx = {
	count: 1
};

function counter() {
	console.log(this.count);
}

console.log(counter().count); // 1