import React from 'react';

export default class UserBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			menuShown: false,
			compact: false,
		};

		window.onscroll = ::this.defineLoginSize();
	}

	render() {
		return (
			<div className='user'>
				<div
					className={'user__login' + (this.state.compact ? 'user__login_compact' : '')}
					onClick={::this.toggleMenuExpansion}>
					{this.props.login}
				</div>

				<div
					className='user__menu'
					style={{display: this.state.menuShown ? 'block' : 'none'}}>
					<div
						className='user__link'
						onClick={::this.toPreferences}>
						Preferences
					</div>

					<div
						className='user__link'
						onClick={::this.toLogout}>
						Logout
					</div>
				</div>
			</div>
		);
	}

	componentDidMount() {
		this.defineLoginSize();
	}

	defineLoginSize() {
		this.setState({
			compact: window.pageYOffset > 100
		});
	}

	toggleMenuExpansion() {
		this.setState({
			menuShown: !this.state.menuShown
		});
	}

	toPreferences() {
		// navigate to preferences
	}

	toLogout() {
		// logout
	}
}


