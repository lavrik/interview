function wrapWithPromise(value) {
    return Promise.resolve(value);
}

(async () => {
    const user = await wrapWithPromise('Alex');
    // Сейчас user имеет тип "any"
})();
